<iframe src="https://onedrive.live.com/embed?cid=EF23DD6D860A8CF3&resid=EF23DD6D860A8CF3%21126&authkey=APquq-fx2zMIUqE&em=2" width="610" height="367" frameborder="0" scrolling="no"></iframe>
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Broader landscape in causal reasoning
We close our tutorial by providing awareness of the landscape of open research and challenges in causal reasoning beyond inference methods.
    
The broader landscape review will touch on the following topics:
* Discovery of causal relationships from data.
* Estimating heterogenous treatment effects
* Machine learning, representations and causal inference.
* Automating causal inference
* Necessary and sufficient causes for events.
    
